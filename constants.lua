-- Constants

function Set (list)
   local set = {}
   for _, l in ipairs(list) do set[l] = true end
   return set
end




g_LiquidBlockTypes = Set({
   E_BLOCK_WATER, E_BLOCK_STATIONARY_WATER,
   E_BLOCK_LAVA, E_BLOCK_STATIONARY_LAVA
})
