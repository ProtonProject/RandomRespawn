function Round(x) return x + 0.5 - (x + 0.5) % 1 end




-- Teleports Player to the highest point. This function is unsafe.
-- Returns if it's suitable or not.
function TeleportPlayerToHighestPoint(a_Player, a_World, a_PointX, a_PointZ)
   local HighestBlockY = a_World:GetHeight(a_PointX, a_PointZ)
   local HighestBlockType = a_World:GetBlock(a_PointX, HighestBlockY, a_PointZ)

   if g_Debug then
      a_Player:SendMessageInfo("PointX = " .. a_PointX)
      a_Player:SendMessageInfo("PointZ = " .. a_PointZ)
      a_Player:SendMessageInfo("HighestBlockY = " .. HighestBlockY)
      a_Player:SendMessageInfo("HighestBlockType = " .. HighestBlockType)
   end

   if g_LiquidBlockTypes[HighestBlockType] then
      if g_Debug then
         a_Player:SendMessageInfo("Unsuitable place to respawn, try again.")
      end

      return false
   else
      if g_Debug then
         a_Player:SendMessageInfo("Suitable place found, moving player")
      end

      a_Player:TeleportToCoords(a_PointX, HighestBlockY + 1, a_PointZ)

      return true
   end
end




-- Teleports Player to a random point of a circle
function TeleportToRandomPoint(a_Player, a_World)
   local PlayerName = a_Player:GetName()

   local a = math.random() * 2 * math.pi
   local r = g_Radius * math.sqrt(math.random())

   local PointX = Round(r * math.cos(a))
   local PointZ = Round(r * math.sin(a))

   local MinChunkX = math.floor((PointX - 25) / 16)
   local MaxChunkX = math.floor((PointX + 25) / 16)
   local MinChunkZ = math.floor((PointZ - 25) / 16)
   local MaxChunkZ = math.floor((PointZ + 25) / 16)
   local Chunks = {}
   for x = MinChunkX, MaxChunkX do
      for z = MinChunkZ, MaxChunkZ do
         table.insert(Chunks, {x, z})
      end
   end

   local DoTeleportPlayer = function (Player)
      local IsSuitable = not TeleportPlayerToHighestPoint(Player, a_World,
                                                          PointX, PointZ)
      if IsSuitable then
         TeleportToRandomPoint(Player, a_World)
      end
   end

   local OnAllChunksAvailable = function ()
      a_World:DoWithPlayer(PlayerName, DoTeleportPlayer)
   end

   a_World:ChunkStay(Chunks, nil, OnAllChunksAvailable)

   return true
end
