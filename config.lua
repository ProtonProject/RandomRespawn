g_IniFile = nil

g_Radius = 1000
g_Worlds = {}
g_Debug = false




function MakeDefaultConfig()
   g_IniFile:AddHeaderComment(" Configuration for Random Respawn")

   g_IniFile:AddKeyName("General")

   g_IniFile:GetValueSetI("General", "Radius", g_Radius)

   g_IniFile:AddKeyName("Worlds")

   local SetDefaultSettingForWorld = function (World)
      local Name = World:GetName()
      g_Worlds[Name] = g_IniFile:GetValueSetB("Worlds", Name, false)
   end

   cRoot:Get():ForEachWorld(SetDefaultSettingForWorld)

   if (not g_IniFile:Flush()) then
      LOGWARNING("Cannot write default configuration file.")
   end
end




function ReadConfig()
   g_Radius = g_IniFile:GetValueI("General", "Radius", g_Radius)
   g_Debug = g_IniFile:GetValueB("General", "Debug", g_Debug)

   local NumberOfWorlds = g_IniFile:GetNumValues("Worlds")

   for v = 0, (NumberOfWorlds - 1) do
      local WorldName = g_IniFile:GetValueName("Worlds", v)
      local IsEnabled = g_IniFile:GetValueSetB("Worlds", WorldName, false)

      g_Worlds[WorldName] = IsEnabled
   end
end





function InitConfig()
   g_IniFile = cIniFile()

   if (g_IniFile:ReadFile("randomrespawn.ini")) then
      ReadConfig()
   else
      MakeDefaultConfig()
   end
end
