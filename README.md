# Random Respawn

Respawns players at random locations of a circle.

## Configuration

```ini
; Configuration for Random Respawn

[General]
; The radius of the circle area where you want players to spawn at
Radius=1000

[Worlds]
; List of worlds where the players should respawn at random points
world=1
world_nether=0
world_the_end=0
```

## Extra

This project is part of [the Proton project][1].

[1]: https://gitlab.com/proton-project
