function Initialize(Plugin)
   Plugin:SetName(g_PluginInfo.Name)
   Plugin:SetVersion(g_PluginInfo.Version)

   InitConfig()

   cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_SPAWNED, OnPlayerSpawn)

   LOG("Initialised " .. g_PluginInfo.Name .. " v" ..
          g_PluginInfo.DisplayVersion)
   return true
end




function OnDisable()
   LOG("Disabling " .. g_PluginInfo.Name .. " v" ..
          g_PluginInfo.DisplayVersion)
end




function OnPlayerSpawn(Player)
   local World = Player:GetWorld()
   if (g_Worlds[World:GetName()]) then
      TeleportToRandomPoint(Player, World)
   end
end
