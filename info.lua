g_PluginInfo = {
      Name = "Random Respawn",
      Version = 1,
      DisplayVersion = "0.1.0",
      Date = "2018-09-02",
      Description = "Randomize respawn location",

      AdditionalInfo = {},
      Commands = {},
      ConsoleCommands = {},
      Permissions = {},
      Categories = {},
}
